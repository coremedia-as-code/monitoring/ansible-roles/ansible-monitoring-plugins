import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/usr/local/monitoring-plugins/icinga2"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/usr/local/monitoring-plugins/icinga2/check_coremedia_capconnection.py",
    "/usr/local/monitoring-plugins/icinga2/check_coremedia_feeder.py",
    "/usr/local/monitoring-plugins/icinga2/check_coremedia_ior.py",
    "/usr/local/monitoring-plugins/icinga2/check_coremedia_licenses.py",
    "/usr/local/monitoring-plugins/icinga2/check_coremedia_publisher.py",
    "/usr/local/monitoring-plugins/icinga2/check_coremedia_runlevel.py",
    "/usr/local/monitoring-plugins/icinga2/check_coremedia_sequencenumbers.py",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file
